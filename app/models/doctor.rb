class Doctor
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String 
	field :dob, type: Date
	field :race, type: String
	field :gender, type: String
	field :mobile_no, type: Integer
	field :city, type: String
	field :image_url, type: String

end
